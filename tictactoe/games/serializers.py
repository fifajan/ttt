from games.models import Game
from rest_framework import serializers


class GameListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Game
        fields = ['id', 'started', 'ended']


class GameDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Game
        fields = ['id', 'started', 'ended', 'board', 'moves',
                  'player_x', 'player_o', 'winner']
