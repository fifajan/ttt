Please perform following steps in order to test this (accurate for Ubuntu 20.04):
---------------------------------------------------------------------------------

(in terminal:)

	$ git clone https://bitbucket.org/fifajan/ttt.git
	$ cd ttt
	$ pip3 install -r requirements.txt
	$ cd tictactoe
	$ python3 manage.py migrate
	$ python3 manage.py runserver localhost:4321

(in separate terminal:)

	$ cd ttt/tictactoe
	$ python3 manage.py create_data         # this creates test users and then they play GAMES_COUNT rangomized games using real API. May take around 30 seconds 
	$ python3 test_2_players.py             # this runs pre recorded game using real API
	$ python3 test_mgmt.py                  # this views infromation via management API using previously created data