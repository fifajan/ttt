from django.contrib.auth.models import User
from django.db.models import Q
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.authentication import BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework import status

from games.models import Game


class MyStats(APIView):
    authentication_classes = [BasicAuthentication]
    permission_classes = [IsAuthenticated]

    def get(self, request, format=None):
        user = request.user
        game_list = []
        games = Game.objects.filter(Q(player_x=user) | Q(player_o=user),
                                    ended__isnull=False,
                                    started__isnull=False)
        for game in games:
            if user == game.winner:
                result = 'won'
            elif not game.winner:
                result = 'draw'
            else:
                result = 'lost'
            game_list.append({
                'id': game.id,
                'played_as': 'x' if user == game.player_x else 'o',
                'result': result,
                'duration': (game.ended - game.started).microseconds,
                'moves': game.moves,
            })
        return Response({'games': game_list,
                         'count': games.count()}, status=status.HTTP_200_OK)


class RankingTable(APIView):
    authentication_classes = [BasicAuthentication]
    permission_classes = [IsAuthenticated]

    def get(self, request, format=None):
        # following should be implemented as some sophisticated ORM query
        # with annotated counts in ideal case. this is
        # "make it work in a limited coding & testing time" approach
        # I believe queries of such type are easier and move native in
        # SQLAlchemy
        user_list = []
        all_users = User.objects.all()
        for user in all_users:
            victories = user.games_won.count()
            defeats = Game.objects.filter(
                        Q(player_x=user) | Q(player_o=user),
                        ~Q(winner=user),
                        winner__isnull=False).count()
            rank = 2 * victories + defeats
            user_list.append({
                'id': user.id,
                'username': user.username,
                'rank': rank,
                'won': victories,
                'lost': defeats,
            })

        return Response({'ranking-table': sorted(user_list,
                                                 key=lambda x: x['rank'],
                                                 reverse=True)},
                        status=status.HTTP_200_OK)



