# Generated by Django 3.1 on 2020-08-12 17:09

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Game',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('started', models.DateTimeField(null=True)),
                ('ended', models.DateTimeField(null=True)),
                ('board_history', models.CharField(blank=True, max_length=200, null=True)),
                ('moves', models.IntegerField(default=0)),
                ('player_o', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='games_as_y', to=settings.AUTH_USER_MODEL)),
                ('player_x', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='games_as_x', to=settings.AUTH_USER_MODEL)),
                ('winner', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='games_won', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
