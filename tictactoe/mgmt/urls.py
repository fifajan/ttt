from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from mgmt import views


urlpatterns = [
    path('my-stats/', views.MyStats.as_view()),
    path('ranking-table/', views.RankingTable.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)
