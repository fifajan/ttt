from random import choice
from requests import get, post
from requests.auth import HTTPBasicAuth as Auth

from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import User
from rest_framework import status

from games.models import Game

BASE_URL = 'http://localhost:4321/games/'
USERNAMES = {'ben', 'cat', 'sam', 'mag', 'joe', 'bob', 'jil',}
GAMES_COUNT = 40


class Command(BaseCommand):
    help = 'Create test users and automatically play N random games'
    def __init__(self, *args, **kwargs):
        super(Command, self).__init__(*args, **kwargs) 
        self.users = dict()


    def handle(self, *args, **options):
        self.create_users()
        self.play_random_games()

    def create_users(self):
        for username in USERNAMES:
            self.users[username] = self.create_user_and_auth(username)
    
    def create_user_and_auth(self, username):    
        user = User.objects.create_user(username=username,
                                        email='{}@{}.xxx'.format(
                                            username,
                                            username),
                                        password=username)

        auth = Auth(username, username)
        return {'user': user, 'auth': auth}

    def play_random_games(self, count=GAMES_COUNT):
        for i in range(count):
            player_x = choice(list(USERNAMES))
            player_o = choice(list(USERNAMES - {player_x,}))

            # new game creation API call:
            resp = post(BASE_URL, auth=self.users[player_x]['auth'],
                        json={'participants': [player_x, player_o]})
            if resp.status_code == status.HTTP_201_CREATED:
                game_id = resp.json()['id']
                print('Created game {}: {} (x) vs {} (o)'.format(
                      game_id, player_x, player_o))
                i = 0
                moves_left = set(range(9))
                while (resp.status_code, resp.json().get('msg')) != (
                        status.HTTP_400_BAD_REQUEST,
                        'This game has ended'):
                    turn_x = i % 2 == 0
                    player = player_x if turn_x else player_o    
                    move = choice(list(moves_left)) if moves_left else 0

                    # player move API call:
                    resp = post('{}{}/'.format(BASE_URL, game_id),
                                auth=self.users[player]['auth'],
                                json={'position': move})
                    print('{} made a move'.format(player))
                    moves_left -= {move,}
                    i += 1
                print('Game ended')
