from requests import get, post
from requests.auth import HTTPBasicAuth as Auth

base_url = 'http://localhost:4321/games/'

sam = Auth('sam', 'sam')
ben = Auth('ben', 'ben')
mag = Auth('mag', 'mag')


print('Ben creates new game:')
resp = post(base_url, auth=ben,
            json={'participants': ['ben', 'sam']})
game_url = '{}{}/'.format(base_url, resp.json()['id'])

print('x (Ben) makes first move: 2')
resp = post(game_url, auth=ben,
            json={'position': 2})

print(resp.status_code, resp.json())

print('x (Ben) tries to make another move. It is not his turn')
resp = post(game_url, auth=ben,
            json={'position': 5})

print(resp.status_code, resp.json())

print('o (Sam) makes first move: 2. x (Ben) already marked that cell')
resp = post(game_url, auth=sam,
            json={'position': 2})

print(resp.status_code, resp.json())

print('o (Sam) makes first correct move: 1')
resp = post(game_url, auth=sam,
            json={'position': 1})

print(resp.status_code, resp.json())

print('x (Ben) makes another move: 5')
resp = post(game_url, auth=ben,
            json={'position': 5})

print(resp.status_code, resp.json())

print('o (Sam) makes another move: 3')
resp = post(game_url, auth=sam,
            json={'position': 3})

print(resp.status_code, resp.json())

print('x (Ben) wants to view current state of the bord:')
resp = get(game_url, auth=ben)
board = resp.json()['board']
print('---\n{}\n{}\n{}\n---'.format(board[:3], board[3:6], board[6:9]))

print('x (Ben) makes final move and wins: 8')
resp = post(game_url, auth=ben,
            json={'position': 8})

print(resp.status_code, resp.json())
