from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import User

from games.models import Game


class Command(BaseCommand):
    help = 'Create few users for testing'

    def handle(self, *args, **options):
        print('Creating users...')
        ben = User.objects.create_user(username='ben',
                                       email='ben@ben.xxx',
                                       password='ben')
        mag = User.objects.create_user(username='mag',
                                       email='mag@mag.xxx',
                                       password='mag')
        sam = User.objects.create_user(username='sam',
                                       email='sam@sam.xxx',
                                       password='sam')

        print('Creating games...')
        Game.objects.create(player_x=ben, player_o=sam, board_history='')
        Game.objects.create(player_x=sam, player_o=ben, board_history='')
        Game.objects.create(player_x=mag, player_o=sam, board_history='')
