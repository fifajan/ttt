from django.contrib import admin
from django.urls import include, path

from games import urls as games_urls
from mgmt import urls as mgmt_urls


urlpatterns = [
    path('admin/', admin.site.urls),
    path('mgmt/', include(mgmt_urls)),
    path('', include(games_urls)),
]
