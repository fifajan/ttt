from datetime import datetime

from django.test import TestCase
from games.models import Game


class GameTestCase(TestCase):
    def test_winner(self):
        kwargs = {
            'started': datetime.now(),
            'ended': datetime.now(),
            'moves': 5,
            'player_x': None,
            'player_o': None,
        }
        kwargs['board_history'] = (
            ' x '
            '   '
            '   '  # move #1
            ' x '
            '  o'
            '   '  # move #2
            ' x '
            ' xo'
            '   '  # move #3
            ' x '
            ' xo'
            '  o'  # move #4
            ' x '
            ' xo'
            ' x '  # move #5
        )
        x_wins_1 = Game.objects.create(**kwargs)
        self.assertEqual(x_wins_1.get_winner(), 'x')

        kwargs['board_history'] = (
            ' x '
            '   '
            '   '  # move #1
            ' x '
            '  o'  
            '   '  # move #2
            ' xx'
            '  o'
            '   '  # move #3
            ' xx'
            ' oo'
            '   '  # move #4
            'xxx'
            ' oo'
            '   '  # move #5
        )

        x_wins_2 = Game.objects.create(**kwargs)
        self.assertEqual(x_wins_2.get_winner(), 'x')

        kwargs['board_history'] = (
            ' x '
            '   '
            '   '  # move #1
            ' x '
            '  o'  
            '   '  # move #2
            ' xx'
            '  o'
            '   '  # move #3
            ' xx'
            ' oo'
            '   '  # move #4
            ' xx'
            ' oo'
            '  x'  # move #5
            ' xx'
            'ooo'
            '  x'  # move #6
        )
        kwargs['moves'] = 6

        o_wins_1 = Game.objects.create(**kwargs)
        self.assertEqual(o_wins_1.get_winner(), 'o')

        kwargs['board_history'] = (
            'xxo'
            'oxx'
            'xoo'
        )
        kwargs['moves'] = 1

        no_winner = Game.objects.create(**kwargs)
        self.assertEqual(no_winner.get_winner(), None)
