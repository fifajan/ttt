from django.core.exceptions import ValidationError
from django.contrib.auth.models import User
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.authentication import BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework import status

from games.models import Game
from games.serializers import GameListSerializer, GameDetailSerializer


class GameList(APIView):
    authentication_classes = [BasicAuthentication]
    permission_classes = [IsAuthenticated]

    def get(self, request, format=None):
        games = Game.objects.all()
        serializer = GameListSerializer(games, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        username_x, username_o = request.data.get('participants')
        player_x = User.objects.get(username=username_x)
        player_o = User.objects.get(username=username_o)
        game = Game.objects.create(player_x=player_x,
                                   player_o=player_o,
                                   board_history='')
        serializer = GameListSerializer(game)
        return Response(serializer.data,
                        status=status.HTTP_201_CREATED)


class GameDetail(APIView):
    authentication_classes = [BasicAuthentication]
    permission_classes = [IsAuthenticated]

    def get(self, request, pk, format=None):
        game = Game.objects.get(pk=pk)
        serializer = GameDetailSerializer(game)
        return Response(serializer.data)

    def post(self, request, pk, format=None):
        user = request.user
        position = request.data.get('position')
        if position is None:
            return Response({'msg': 'position is needed'},
                status=status.HTTP_400_BAD_REQUEST)

        game = Game.objects.get(id=pk)
        if game.ended:
            return Response({'msg': 'This game has ended'},
                status=status.HTTP_400_BAD_REQUEST)
        try:
            game.make_move(position, user)
        except ValidationError as ex:
            return Response({'msg': ex.message},
                status=status.HTTP_400_BAD_REQUEST)

        game.refresh_from_db()
        if game.winner == user:
            return Response({'msg': 'You won this game. Congrats!'},
                status=status.HTTP_200_OK)

        if game.ended and game.winner is None:
            return Response({'msg': 'Game ended with draw'},
                status=status.HTTP_200_OK)

        return Response({'msg': 'You made a move'},
                status=status.HTTP_200_OK)


class GameHistory(APIView):
    authentication_classes = [BasicAuthentication]
    permission_classes = [IsAuthenticated]

    def get(self, request, pk):
        game = Game.objects.get(id=pk)

        # TODO fix bug with escaped new line character
        return Response(game.to_str(), content_type='text/plain',
                        status=status.HTTP_200_OK)
