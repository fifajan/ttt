from requests import get, post
from requests.auth import HTTPBasicAuth as Auth
from pprint import PrettyPrinter

pp = PrettyPrinter(indent=2).pprint

stats_url = 'http://localhost:4321/mgmt/my-stats/'
rank_url = 'http://localhost:4321/mgmt/ranking-table/'

sam = Auth('sam', 'sam')
cat = Auth('cat', 'cat')
ben = Auth('ben', 'ben')
mag = Auth('mag', 'mag')

print('Ben views his stats:')
resp = get(stats_url, auth=ben)
pp(resp.json())

print('Sam views his stats:')
resp = get(stats_url, auth=sam)
pp(resp.json())

print('Cat views her stats:')
resp = get(stats_url, auth=cat)
pp(resp.json())

print('Mag views her stats:')
resp = get(stats_url, auth=mag)
pp(resp.json())

print('Cat views ranking table:')
resp = get(rank_url, auth=cat)
pp(resp.json())

