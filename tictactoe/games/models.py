from datetime import datetime

from django.db import models
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError


LINE_INDICES = [
    [0, 1, 2],  # top horizontal
    [3, 4, 5],  # middle horizontal
    [6, 7, 8],  # bottop horizontal
    [0, 3, 6],  # left vertial
    [1, 4, 7],  # middle vertical
    [2, 5, 8],  # right vertical
    [0, 4, 8],  # left to right diagonal
    [2, 4, 6],  # right to left diagonal
]

BOARD_TEMPLATE = (
    '+---+---+---+\n'
    '| {} | {} | {} |\n'
    '+---+---+---+\n'
    '| {} | {} | {} |\n'
    '+---+---+---+\n'
    '| {} | {} | {} |\n'
    '+---+---+---+\n'
)


class Game(models.Model):
    started = models.DateTimeField(null=True)
    ended = models.DateTimeField(null=True)
    board_history = models.CharField(max_length=200, null=True, blank=True)
    moves = models.IntegerField(default=0)
    player_x = models.ForeignKey(User, on_delete=models.SET_NULL, null=True,
                                 related_name='games_as_x')
    player_o = models.ForeignKey(User, on_delete=models.SET_NULL, null=True,
                                 related_name='games_as_y')
    winner = models.ForeignKey(User, on_delete=models.SET_NULL, null=True,
                                 related_name='games_won')

    def get_winner(self):
        board = self.board()
        for line in LINE_INDICES:
            char = board[line[0]]
            if char != ' ' and all(board[i] == char for i in line):
                return char

    def board(self, move=-1):  # move == -1 means the last state of the board
        if not self.board_history:
            return ' ' * 9
        if move == -1:
            move = self.moves - 1
        start = 9 * move
        end = start + 9
        return self.board_history[start:end]

    def possible_moves(self):
        board = self.board()
        return [i for i in range(9) if board[i] == ' ']

    def make_move(self, position, user):
        # validation of this move:
        if user not in (self.player_x, self.player_o):
            raise ValidationError('You are not a player in this game.')
        if (self.moves % 2 == 0 and user != self.player_x) or (
                self.moves % 2 == 1 and user != self.player_o):
            raise ValidationError('It is not your turn.')
        if position not in self.possible_moves():
            raise ValidationError('Your move is impossible.')

        # recording move to history
        user_char = 'x' if user == self.player_x else 'o'
        board = list(self.board())
        board[position] = user_char
        self.board_history += ''.join(board)
        self.moves += 1
        if self.moves == 1:
            self.started = datetime.now()

        # checking for a winner
        if self.get_winner():
            self.winner = user
            self.ended = datetime.now()

        # checking for a draw
        if ' ' not in board:
            self.ended = datetime.now()

        self.save()

    def to_str(self):
        result = ''
        for i in range(self.moves):
            result += BOARD_TEMPLATE.format(*self.board(move=i)) + '\n'
        return result
